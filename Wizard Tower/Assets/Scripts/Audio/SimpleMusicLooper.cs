﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMusicLooper : MonoBehaviour {

    public AudioSource musicSource;
    public AudioClip music;
    public float timeToWait = 30;
    public float fadeInSpeed = 1f;
    public float fadeOutSpeed = 1f;
    public float maxMusicVolume = 1f;
    public float timeToStartFading = 0f;
    public float startTime = 0f;
    private bool _fadingIn = true;
    private bool _waiting = false;
    

    // Start is called before the first frame update
    void Start() {
        musicSource.loop = false;
        musicSource.Play();
        musicSource.time = startTime;
    }

    // Update is called once per frame
    void Update() {
        if (!_waiting) {
            DetectMusicProgress();
            FadeIn();
            FadeOut();
        }
    }

    private void DetectMusicProgress() {
        float timeUntilEnd = music.length - musicSource.time;
        if (timeUntilEnd <= timeToStartFading && _fadingIn) {
            _fadingIn = false;
        }
    }

    private void FadeIn() {
        if (musicSource.volume < maxMusicVolume && _fadingIn) {
            musicSource.volume += Time.deltaTime * fadeInSpeed;
        }
    }

    private void FadeOut() {
        if (musicSource.volume > 0 && !_fadingIn) {
            musicSource.volume -= Time.deltaTime * fadeOutSpeed;
            StartCoroutine(WaitToStart());
        }
    }

    private IEnumerator WaitToStart() {
        yield return new WaitForSeconds(timeToWait);
        musicSource.time = 0;
        _fadingIn = true;
        _waiting = false;
        musicSource.Play();
    }
}
