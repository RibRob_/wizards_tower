﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetLocation : MonoBehaviour {

    public float checkInterval = 0.3f;

    public Transform ownTransform;
    public Transform loadTransform;
    public Transform towerTransform;

    public int towerIndex = 0;
    public int loadIndex = 0;

    public string towerSceneName = "scene name";

    public bool testingTower = true;
    private void Awake() {

        Scene currentScene = SceneManager.GetSceneByName(towerSceneName);

        if (currentScene.buildIndex == towerIndex || testingTower) {
            TowerTransform();
        }
        else if (currentScene.buildIndex == loadIndex) {
            LoadTransform();
        }
        else {
            LoadTransform();
        }
    }

    // Start is called before the first frame update
    void Start() {
        
    }

    private void OnLevelWasLoaded(int level) {
        if (level == loadIndex) {
            LoadTransform();
        }
        else if (level == towerIndex) {
            TowerTransform();
        }
    }

    public void TowerTransform() {
        ownTransform.position = towerTransform.position;
    }

    public void LoadTransform() {
        ownTransform.position = loadTransform.position;
    }
}
