﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSplash : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        SceneManager.LoadScene("Splash Scene", LoadSceneMode.Additive);
        
    }
}
