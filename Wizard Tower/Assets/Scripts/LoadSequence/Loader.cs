﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {

    public string loadingSceneName = "MainMenu";
    public string unloadingSceneName = "";
    public float waitTime = 0f;
    public bool additive = true;
    public bool unloadCurrentScene = true;
    public bool load = false;

    private void Update() {
        if (load) {
            load = false;
            StartCoroutine("Load");
        }
    }

    public void StartLoad() {
        load = true;
    }

    IEnumerator Load() {
        yield return new WaitForSeconds(waitTime);

        if (additive) {
            SceneManager.LoadScene(loadingSceneName, LoadSceneMode.Additive);

            if (unloadCurrentScene) {
                SceneManager.UnloadSceneAsync(unloadingSceneName);
            }
        }
        else {
            SceneManager.LoadScene(loadingSceneName, LoadSceneMode.Single);
        }
    }




}
