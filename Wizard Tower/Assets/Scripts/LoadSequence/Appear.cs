﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Appear : MonoBehaviour {

    [Header("Settings")]
    public GameObject[] objectsToAppear = new GameObject[1];
    public float time = 0f;
    public bool doesTrigger = true;
    public UnityEvent TriggerAfterAppear; 

    // Start is called before the first frame update
    void Start() {
        StartCoroutine("MakeAppear");
    }

    private IEnumerator MakeAppear() {
        Debug.Log("Starting to make appear");
        yield return new WaitForSeconds(time);
        Debug.Log("Making appear...");
        foreach (GameObject go in objectsToAppear) {
            go.SetActive(true);
        }

        if (doesTrigger) {
            TriggerAfterAppear.Invoke();
        }
    }
}
