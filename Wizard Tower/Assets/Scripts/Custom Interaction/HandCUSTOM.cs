﻿//Rob Harwood
//Followed this tutorial: https://www.youtube.com/watch?v=HnzmnSqE-Bc
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class HandCUSTOM : MonoBehaviour {


    public SteamVR_Action_Boolean triggerAction = null;
    public SteamVR_Action_Boolean squeezeAction = null;

    public GameObject model;

    private SteamVR_Behaviour_Pose _pose = null;
    private FixedJoint _joint = null;

    public InteractableCUSTOM currentInteractable;
    [SerializeField] private List<InteractableCUSTOM> _contactInteractables = new List<InteractableCUSTOM>();
    [SerializeField] private Collider _currentCollider;

    private void Awake() {
        _pose = GetComponent<SteamVR_Behaviour_Pose>();
        _joint = GetComponent<FixedJoint>();
    }


    // Start is called before the first frame update
    private void Start() {
        
    }

    // Update is called once per frame
    private void Update() {
        DetectPickUp();
        UseInteractable();

    }

    private void DetectPickUp() {
        //Down
        if (squeezeAction.GetStateDown(_pose.inputSource)) {
            print(_pose.inputSource + " Trigger down");



            PickUp();
            return;//Must return, otherwise it will drop immediately
        }

        //Up
        if (squeezeAction.GetStateDown(_pose.inputSource)) {
            print(_pose.inputSource + " Trigger up");
            Drop();
            return;
        }
    }

    private void UseInteractable() {
        if (currentInteractable && triggerAction.GetStateDown(_pose.inputSource)) {
            currentInteractable.Action();
        }
    }


    private void OnTriggerEnter(Collider other) {
        if (!other.gameObject.CompareTag("Interactable")) {
            return;
        }

        _contactInteractables.Add(other.gameObject.GetComponent<InteractableCUSTOM>());
    }

    private void OnTriggerExit(Collider other) {
        if (!other.gameObject.CompareTag("Interactable")) {
            return;
        }

        _contactInteractables.Remove(other.gameObject.GetComponent<InteractableCUSTOM>());
    }

    public void PickUp() {
        //Get Nearest
        currentInteractable = GetNearestInteractable();

        //Null check
        if (!currentInteractable) {
            return;
        }

        //Already held check
        if (currentInteractable._activeHand) {
            currentInteractable._activeHand.Drop();
        }

        //Position
        if (currentInteractable) {
            currentInteractable.ApplyOffset(transform);
            _currentCollider = currentInteractable.GetComponent<Collider>();
            if (_currentCollider) {
                _currentCollider.enabled = false;
            }
        }

        //Attach
        if (currentInteractable) {
            Rigidbody targetBody = currentInteractable.GetComponent<Rigidbody>();
            _joint.connectedBody = targetBody;
        }

        //Set active hand
        if (currentInteractable) {
            currentInteractable._activeHand = this;
            currentInteractable.handTag = this.tag;
            DontDestroyOnLoad(currentInteractable.gameObject);
        }

        //Disable model
        if (currentInteractable) {
            model.SetActive(false);
        }
    }

    public void Drop() {
        print("Drop");
        model.SetActive(true);

        //Null check
        if (!currentInteractable) {
            return;
        }

        if (_currentCollider) {
            _currentCollider.enabled = true;
        }

        //Apply velocity
        Rigidbody targetBody = currentInteractable.GetComponent<Rigidbody>();
        targetBody.velocity = _pose.GetVelocity();
        targetBody.angularVelocity = _pose.GetAngularVelocity();

        //Detach
        _joint.connectedBody = null;

        //Clear
        currentInteractable.handTag = "";
        currentInteractable._activeHand = null;
        currentInteractable = null;
        _currentCollider = null;

        //Enable model
        model.SetActive(true);
    }

    private InteractableCUSTOM GetNearestInteractable() {
        InteractableCUSTOM nearest = null;
        float minDistance = float.MaxValue;
        float distance = 0.0f;

        foreach (InteractableCUSTOM interactable in _contactInteractables) {
            distance = (interactable.transform.position - transform.position).sqrMagnitude;

            if (distance < minDistance) {
                minDistance = distance;
                nearest = interactable;
            }
        }

        return nearest; 
    }
}
