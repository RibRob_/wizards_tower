﻿//Rob Harwood
//Followed this tutorial: https://www.youtube.com/watch?v=ryfUXr5yvKw
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody))]
public class InteractableCUSTOM : MonoBehaviour {
    
    public Vector3 offsetPositon = Vector3.zero;
    public Vector3 offsetRotation = Vector3.zero;

    public HandCUSTOM _activeHand = null;
    public GameObject lastModel;
    public string handTag = "";

    private void Start() {
        if (handTag != "") {
            Scene currentScene = SceneManager.GetActiveScene();
            _activeHand = GameObject.FindGameObjectWithTag(handTag).GetComponent<HandCUSTOM>();
            _activeHand.currentInteractable = this;
            _activeHand.PickUp();
        }
    }

    public virtual void Action() {
        print("Action");
    }

    public void ApplyOffset(Transform hand) {
        transform.SetParent(hand);
        transform.localRotation = Quaternion.identity;
        transform.localPosition = offsetPositon;
        transform.SetParent(null);
    }

    
    private void OnLevelWasLoaded(int level) {
        if (SceneManager.GetActiveScene().name == "SealedCaverns" && handTag == "") {
            Destroy(this.gameObject);
        }
    }
}
