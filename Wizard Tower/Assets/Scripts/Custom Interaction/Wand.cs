﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wand : InteractableCUSTOM {

    public ObjectPool fireballPool;
    public Transform spawnPoint;

    public override void Action() {
        fireballPool.Poof(spawnPoint);
    }
}
