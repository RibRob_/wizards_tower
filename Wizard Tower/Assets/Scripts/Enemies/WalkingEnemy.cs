﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingEnemy : MonoBehaviour {

    public int pointValue = 10;
    public int damage = 10;
    public Transform playerTransform;
    public float minDistance = 1f;
    public float moveSpeed = 1f;
    public float smoothSpeed = 0.1f;
    public float attackDelay = 5f;
    public Player playerScript;
    public Animator animator;
    public bool delayComplete = true;

    // Start is called before the first frame update
    private void Start() {
        
    }

    // Update is called once per frame
    private void FixedUpdate() {
        Move();
    }

    private void Move() {
        this.transform.LookAt(playerTransform);//Look at the player
        Vector3 lookDirectrion = this.transform.eulerAngles;
        lookDirectrion.x = 0;
        this.transform.eulerAngles =  lookDirectrion;
        float distanceFromPlayer = Vector3.Distance(this.transform.position, playerTransform.position);

        Vector3 desiredPosition = new Vector3();
        desiredPosition = transform.position;

        if (distanceFromPlayer > minDistance) { //Move towards Player
            animator.SetBool("walking", true);
            desiredPosition = Vector3.MoveTowards(this.transform.position, playerTransform.position, moveSpeed);
        }
        else { //Attack Player
            animator.SetBool("walking", false);
            if (delayComplete) {
                StartCoroutine("AttackAndWait");
                delayComplete = false;
            }
        }

        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        smoothedPosition.y = 0;
        this.transform.position = smoothedPosition;
    }

    private IEnumerator AttackAndWait() {
        //Play skeleton animation
        animator.Play("Attack");
        yield return new WaitForSeconds(attackDelay);
        delayComplete = true;
    }

    public void Die() {
        //Play sound
        //spawn temporary bones
        playerScript.KilledEnemy(pointValue);
        this.gameObject.SetActive(false);
    }

    public void Hit() {
        playerScript.TakeDamage(damage);
    }
}
