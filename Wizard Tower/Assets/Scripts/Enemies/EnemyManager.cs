﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    [Header ("Object Pools")]
    public ObjectPool floatingSkullPool;
    public ObjectPool skeletonPool;

    [Header("Wait Times")]
    public float waitTimeToSpawn = 4f;

    [Header("Randomized Spawn Location Inputs")]
    public int xMaxSpawnDistance = 30;
    public int xMinSpawnDistance = -30;
    public int yMaxSpawnDistance = 8;
    public int yMinSpawnDistance = 1;
    public int zMaxSpawnDistance = 30;
    public int zMinSpawnDistance = -30;
    public int maxWaitTimeToSpawnSkull = 6;
    public int maxWaitTimeToSpawnSkeleton = 6;

    private static readonly object syncLock = new object();
    private static readonly System.Random random = new System.Random();

    // Start is called before the first frame update
    void Start() {
        StartCoroutine("WaitToSpawnSkull");
        StartCoroutine("WaitToSpawnSkeleton");
    }

    // Update is called once per frame
    void Update() {
        
    }

    IEnumerator WaitToSpawnSkull() {
        yield return new WaitForSeconds(waitTimeToSpawn);

        float x = 0;
        float y = 0;
        float z = 0;

        lock (syncLock) {
            x = random.Next(xMinSpawnDistance, xMaxSpawnDistance);
            y = random.Next(yMinSpawnDistance, yMaxSpawnDistance);
            z = random.Next(zMinSpawnDistance, zMaxSpawnDistance);
            waitTimeToSpawn = random.Next(0, maxWaitTimeToSpawnSkull);
        }

        Vector3 location = new Vector3(x, y, z);
        GameObject thisEnemy;
        floatingSkullPool.Poof(location, out thisEnemy);
        thisEnemy.GetComponent<Damagable>().FullHeal();
        StartCoroutine("WaitToSpawnSkull");
    }

    IEnumerator WaitToSpawnSkeleton() {
        yield return new WaitForSeconds(waitTimeToSpawn);

        float x = 0;
        float y = 0;
        float z = 0;

        lock (syncLock) {
            x = random.Next(xMinSpawnDistance, xMaxSpawnDistance);
            y = 0;
            z = random.Next(zMinSpawnDistance, zMaxSpawnDistance);
            waitTimeToSpawn = random.Next(0, maxWaitTimeToSpawnSkeleton);
        }

        Vector3 location = new Vector3(x, y, z);
        GameObject thisEnemy;
        skeletonPool.Poof(location, out thisEnemy);
        thisEnemy.GetComponent<Damagable>().FullHeal();
        StartCoroutine("WaitToSpawnSkeleton");
    }
}
