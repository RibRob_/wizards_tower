﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingEnemy : MonoBehaviour {

    [Header("Settings")]
    public float minDistance = 10f;
    public float smoothSpeed = 5f;
    public float moveSpeed = 4f;
    public float circleSpeed = 4f;
    public float floatSpeed = 1;
    public float floatAmplitude = 1;
    public float startingY = 0;
    public int pointValue = 20;
    public Player playerScript;
    public Transform playerTransform;
    public Vector3 rotationOffset;

    private static readonly object syncLock = new object();
    private static readonly System.Random random = new System.Random();

    // Start is called before the first frame update
    private void Start() {
        startingY = transform.position.y;

        lock (syncLock) { //Prevents random.next from repeating itself.
            floatSpeed += (float)random.NextDouble();
            floatAmplitude = (float)random.NextDouble();
            floatAmplitude = Mathf.Clamp(floatAmplitude, 0.08f, 0.12f);
            moveSpeed += (float)random.NextDouble();
            circleSpeed += (float)random.NextDouble();
        }

    }

    // Update is called once per frame
    private void FixedUpdate() {
        Move();
    }

    private void Move() {

        this.transform.LookAt(playerTransform);//Look at the player
        this.transform.eulerAngles = new Vector3 (0, transform.eulerAngles.y, transform.eulerAngles.z); //Prevent X from rotating
        this.transform.eulerAngles += rotationOffset;
        float distanceFromPlayer = Vector3.Distance(this.transform.position, playerTransform.position);

        Vector3 desiredPosition = new Vector3();
        desiredPosition = transform.position;

        if (distanceFromPlayer > minDistance) { //Move towards Player
            desiredPosition = Vector3.MoveTowards(this.transform.position, playerTransform.position, moveSpeed);
        }
        else { //Cirlce Player
            transform.RotateAround(playerTransform.position, Vector3.up, circleSpeed * Time.deltaTime);
            desiredPosition = transform.position;
        }

        desiredPosition.y = (startingY + Mathf.Sin(Time.time * floatSpeed) * floatAmplitude);
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        this.transform.position = smoothedPosition;
    }

    public void Die() {
        playerScript.KilledEnemy(pointValue);
        this.gameObject.SetActive(false);
    }
}
