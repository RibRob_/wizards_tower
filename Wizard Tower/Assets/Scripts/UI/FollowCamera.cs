﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform pivotPointTransform;
    public Transform cameraTransform;

    // Start is called before the first frame update
    void Start(){
        
    }

    // Update is called once per frame
    void Update() {
        Vector3 newRotation = new Vector3(0, cameraTransform.rotation.y, 0);
        pivotPointTransform.rotation = new Quaternion(0, cameraTransform.rotation.y, 0, 0);
        //pivotPointTransform.eulerAngles = newRotation;
    }
}
