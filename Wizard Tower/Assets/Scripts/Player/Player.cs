﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Player : MonoBehaviour
{

    public int currentHealth = 100;
    public int maxHealth = 100;
    public int score = 0;
    public Image healthRing;
    public TextMeshProUGUI scoreText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update() {
        CheckHealth();
        UpdateHealthRing();
        UpdateScoreText();
    }

    private void CheckHealth() {
        if (currentHealth <= 0) {
            //Game over sequence
            //Play losing sound
            //Go back to wizard tower
            SceneManager.LoadScene("Wizard Tower");
        }
        else if (currentHealth > maxHealth) {
            currentHealth = maxHealth;
        }
    }

    private void UpdateHealthRing() {
        float radialValue = (float)currentHealth / (float)maxHealth;
        healthRing.fillAmount = radialValue;
    }

    private void UpdateScoreText() {
        scoreText.text = score.ToString();
    }

    public void KilledEnemy(int newPoints) {
        score += newPoints;
    }

    public void TakeDamage(int damageDealt) {
        currentHealth -= damageDealt;
        float radialValue = (float)currentHealth / (float)maxHealth;
    }
}
