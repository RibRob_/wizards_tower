﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectFallenObjects : MonoBehaviour {

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Interactable") {
            Debug.Log("An object fell: " + collision.gameObject.name);
            collision.gameObject.SendMessage("TellFallen");
        }
    }
}
