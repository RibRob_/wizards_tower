﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour {

    public Renderer ownRenderer;
    public GameObject origin;
    public bool hasFallen = false;
    public Rigidbody ownRigidbody;
    public Quaternion originalRotation;
    public Collider collider;
    public ObjectPool poofParticlePool;

    // Start is called before the first frame update
    void Start() {
        originalRotation = this.transform.rotation;
    }

    private void FixedUpdate() {
        //print("hasfallen: " + hasFallen + "\n" +
        //      "!isVisible: " + !ownRenderer.isVisible);
        if (hasFallen && !ownRenderer.isVisible) {
            ResetToOrigin();
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.other.tag == "Floor") {
            print("I have fallen!");
            hasFallen = true;
        }
    }

    public void TellFallen() {
        hasFallen = true;
    }

    public void ForceReset() {
        ResetToOrigin();
    }

    private void ResetToOrigin() {
        this.transform.position = origin.transform.position;
        this.transform.rotation = originalRotation;
        ownRigidbody.velocity = new Vector3(0, 0, 0);
        poofParticlePool.Poof(origin.transform);
        hasFallen = false;
    }
}
