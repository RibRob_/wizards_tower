﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : MonoBehaviour {

    public Transform highPoint;
    public Transform lowPoint;
    public Transform potionSpawnPoint;
    public ObjectPool potionLiquidPool;
    public float waitUntilPouringAgain = 0.1f;
    public bool pouring = false;
    public bool waiting = false;
    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        CheckPoints();
        PourPotion();
    }

    private void CheckPoints() {
        if (highPoint.position.y < lowPoint.position.y) {
            pouring = true;
        }
        else {
            pouring = false;
        }
    }

    private void PourPotion() {
        if (!waiting && pouring) {
            potionLiquidPool.Poof(potionSpawnPoint.position);
            StartCoroutine(WaitToPour());
        }
    }

    private IEnumerator WaitToPour() {
        waiting = true;
        yield return new WaitForSeconds(waitUntilPouringAgain);
        waiting = false;
    }
}
