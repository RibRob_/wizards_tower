﻿//Rob Harwood
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DetectPotion : MonoBehaviour {

    public GameObject cameraRig;
    public Player playerScript;
    public AudioSource gulpSource;
    public int timesScenesHaveLoaded = 0;

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Potion Liquid") {
            collision.gameObject.SetActive(false);
            if (!gulpSource.isPlaying) {
                gulpSource.Play();
            }
            if (playerScript) {
                playerScript.currentHealth += 2;
            }
        }
    }

    private void OnLevelWasLoaded(int level) {
        timesScenesHaveLoaded++;
        try {
            if (timesScenesHaveLoaded >= 2) {
                cameraRig.SetActive(false);
                return;
                //Destroy(cameraRig);
            }
            else if (timesScenesHaveLoaded == 1) {
                Scene sealedCavernsScene = SceneManager.GetSceneByBuildIndex(4);
                GameObject[] rootGameObjects = sealedCavernsScene.GetRootGameObjects();
                foreach (GameObject go in rootGameObjects) {
                    if (go.name == "Player") {
                        playerScript = go.GetComponent<Player>();
                    }
                }
            }
        }
        catch (Exception e) {
            Destroy(this.gameObject);
        }
    }
}
