﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Float : MonoBehaviour {

    public float speed = 1f;
    public float amplitude = 1f;
    public Vector3 startPosition;

    // Start is called before the first frame update
    void Start() {
        startPosition = this.transform.position;
    }


    // Update is called once per frame
    private void FixedUpdate() {
        //Vector3 newPosition = new Vector3 (transform.position.x,
        //                                      startPosition.y + Mathf.Sin(Time.time * ampl),
        //                                      transform.position.z);
        //newPosition
        this.transform.position = new Vector3(transform.position.x,
                                              (startPosition.y + Mathf.Sin(Time.time * speed) * amplitude),
                                              transform.position.z);
    }
}
