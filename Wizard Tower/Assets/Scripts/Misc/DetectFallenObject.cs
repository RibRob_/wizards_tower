﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectFallenObject : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        Reset fallenResetalbe = other.GetComponent<Reset>();
        if (fallenResetalbe) {
            fallenResetalbe.ForceReset();
            Debug.Log(other.name + " HAS FALLEN TO LIMIT, RESETTING LOCATION");
        }
    }
}
