﻿//Rob Harwood
//Followed tutorial: https://www.youtube.com/watch?v=mGqHpYJWYx8
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Button : MonoBehaviour {

    public Transform parent;
    public Transform pointA;
    public Transform pointB;

    public Vector3 offset;

    public UnityEvent hitA;
    public UnityEvent hitB;
    public UnityEvent releasedA;
    public UnityEvent releasedB;

    private int _state = 0;
    private int _previousState = 0;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        SetPosition();
        ChangeState();
        ObserveState();
    }

    private void SetPosition() {
        if (transform.position.y > pointA.transform.position.y) {
            transform.position = pointA.transform.position;
        }
        else if (transform.position.y < pointB.transform.position.y) {
            transform.position = pointB.transform.position;
        }
    }

    private void ChangeState() {
        if (transform.position == pointA.position) {
            _state = 1;
        }
        else if (transform.position == pointB.position) {
            _state = 2;
        }
        else {
            _state = 0;
        }
    }

    private void ObserveState() {
        if (_state == 1 && _previousState == 0) {
            hitA.Invoke();
        }
        else if (_state == 2 && _previousState == 0) {
            hitB.Invoke();
        }
        else if (_state == 0 && _previousState == 1) {
            releasedA.Invoke();
        }
        else if (_state == 0 && _previousState == 2) {
            releasedB.Invoke();
        }

        _previousState = _state;
    }
}
