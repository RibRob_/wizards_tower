﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLayer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        other.gameObject.layer = 8;
    }

    private void OnTriggerExit(Collider other) {
        other.gameObject.layer = 0;
    }
}
