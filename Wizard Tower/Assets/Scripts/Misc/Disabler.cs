﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disabler : MonoBehaviour {

    public string methodName = "DisableAfterTime";
    public float timer = 3f;
    public bool disableOnCollision = false;

    // Start is called before the first frame update
    void Start() {
        StartCoroutine("DisableAfterTime");
    }

    private IEnumerator DisableAfterTime() {
        yield return new WaitForSeconds(timer);
        this.gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision) {
        if (disableOnCollision) {
            this.gameObject.SetActive(false);
        }
    }
}
