﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour {

    public string levelName = "";

    public GameObject playerRig;
    public HandCUSTOM hand1;
    public HandCUSTOM hand2;

    private GameObject _hand1MagicObject;
    private GameObject _hand2MagicObject;


    public void Load() {
        //_hand1MagicObject = hand1.currentInteractable.gameObject;
        //_hand2MagicObject = 
        DontDestroyOnLoad(playerRig);
        DontDestroyOnLoad(hand1.currentInteractable.gameObject);
        DontDestroyOnLoad(hand2.currentInteractable.gameObject);
        SceneManager.LoadScene(levelName);
    }
}
