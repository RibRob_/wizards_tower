﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

    public GameObject[] objectPool = new GameObject[0];
    public Transform rotationToFollow;
    public int currentIndex = 0;

    // Start is called before the first frame update
    void Start() {
        
    }

    private void Update() {
        CheckIndex();
    }

    public void Poof(Transform location) {
        Exists();
        objectPool[currentIndex].transform.position = location.position;
        if (rotationToFollow) {
            objectPool[currentIndex].transform.forward = rotationToFollow.forward;
        }
        objectPool[currentIndex].SetActive(true);
        currentIndex++;
    }

    public void Poof(Vector3 location) {
        Exists();
        objectPool[currentIndex].transform.position = location;
        objectPool[currentIndex].SetActive(true);
        currentIndex++;
    }

    public void Poof(Vector3 location, out GameObject poolObject) {
        Exists();
        poolObject = objectPool[currentIndex];
        objectPool[currentIndex].transform.position = location;
        objectPool[currentIndex].SetActive(true);
        currentIndex++;
    }

    private void Exists() {
        int passes = 0;
        bool exists;
        do {
            CheckIndex();
            exists = objectPool[currentIndex].activeInHierarchy;
            if (exists) {
                passes++;
                currentIndex++;
                CheckIndex();

                if (passes > 10) {
                    break;
                    Debug.Log("Had to break loop, all objects in use.");
                }
            }
        } while (exists);
    }

    private void CheckIndex() {
        if (currentIndex >= objectPool.Length) {
            currentIndex = 0;
        }
    }

}
