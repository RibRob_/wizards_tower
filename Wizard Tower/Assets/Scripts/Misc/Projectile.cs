﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float speed = 1f;
    public int damage = 5;
    public Rigidbody rigidbody;

    // Start is called before the first frame update
    void Start() {
        
    }

    private void FixedUpdate() {
        //this.transform.Translate(this.transform.forward * Time.deltaTime * speed);
        rigidbody.velocity = ((transform.forward) * Time.deltaTime * speed);
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag != "Interactable") {
            this.gameObject.SetActive(false);
        }

        if (collision.gameObject.tag == "Enemy") {
            collision.gameObject.GetComponent<Damagable>().TakeDamage(damage);
            this.gameObject.SetActive(false);
        }
    }
}
