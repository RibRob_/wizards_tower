﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Damagable : MonoBehaviour {

    public int currentHealth = 10;
    public int maxHealth = 10;
    public UnityEvent died;

    // Start is called before the first frame update
    void Start() {
        
    }

    private void Update() {
        CheckHealth();
    }

    public void TakeDamage(int damage) {
        currentHealth -= damage;
        CheckHealth();
    }

    private void CheckHealth() {
        if (currentHealth <= 0) {
            died.Invoke();
        }
    }

    public void FullHeal() {
        currentHealth = maxHealth;
    }
}
